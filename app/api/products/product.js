import AppHttp from "../AppHttp";

const getProduct = async(id) => {
    const http = await AppHttp();
    const { data } = await http.get(`/products/${id}`);
    return data;
};

export { getProduct };