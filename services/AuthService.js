import CookiesService from "./CookiesService";

class AuthService {
    handleLoginSuccess(accessToken) {
        const options = { path: "/" };
        CookiesService.set("access_token", accessToken, options);
    }

    handleLogoutSuccess() {
        CookiesService.remove("access_token");
    }
}

export default new AuthService();