module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        lightGreen: "#69fdc9",
        darkGreen: "#44caca",
        buttonFrom: "#ff886b",
        buttonTo: "#ff4164",
        sliderDarkBg: "#349a98",
        bbbbbb: "#bbbbbb",
      },
      boxShadow: {
        productBox: "3px 2px 16px -2px rgba(0,0,0,0.43)",
        ImageBox: "0px 14px 16px -2px rgba(68,202,202,0.43)",
        buttonAddToCart: "0px 9px 16px 3px rgba(255,65,100,0.43)",
      },

      fontFamily: {
        body: `Oswald`,
      },
    },
  },
  plugins: [],
};
