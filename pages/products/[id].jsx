import { NextSeo } from "next-seo";
import Head from "next/head";
import React from "react";
import { getProduct } from "../../app/api/products/product";
import ProductItem from "../../components/Product";

export default function Product({ productData }) {
  console.log("productData", productData);
  return (
    <>
      <NextSeo
        title={productData.title}
        description={productData.description}
        canonical="https://vitrin.me/"
        // openGraph={{
        //   url: "https://www.url.ie/a",
        //   title: "Open Graph Title",
        //   description: "Open Graph Description",
        //   images: [
        //     {
        //       url: "https://www.example.ie/og-image-01.jpg",
        //       width: 800,
        //       height: 600,
        //       alt: "Og Image Alt",
        //       type: "image/jpeg",
        //     },
        //   ],
        //   site_name: "SiteName",
        // }}
        // twitter={{
        //   handle: "@handle",
        //   site: "@site",
        //   cardType: "summary_large_image",
        // }}
      />
      <ProductItem productData={productData} />
    </>
  );
}

export async function getServerSideProps(context) {
  const { params } = context;
  const { id } = params;

  let data = await getProduct(id);
  return {
    props: { productData: data },
  };
}
