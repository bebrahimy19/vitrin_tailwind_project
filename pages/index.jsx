import { NextSeo } from "next-seo";
import Head from "next/head";
import HomePage from "../components/HomePage/HomePage";

export default function Home({}) {
  return (
    <div>
      <NextSeo
        title="Vitrin"
        description="طراحی سایت | تا 200% فروش بیشتر با ویترین | Vitrin"
        canonical="https://vitrin.me/"
        // openGraph={{
        //   url: "https://www.url.ie/a",
        //   title: "Open Graph Title",
        //   description: "Open Graph Description",
        //   images: [
        //     {
        //       url: "https://www.example.ie/og-image-01.jpg",
        //       width: 800,
        //       height: 600,
        //       alt: "Og Image Alt",
        //       type: "image/jpeg",
        //     },
        //   ],
        //   site_name: "SiteName",
        // }}
        // twitter={{
        //   handle: "@handle",
        //   site: "@site",
        //   cardType: "summary_large_image",
        // }}
      />

      <HomePage />
    </div>
  );
}
