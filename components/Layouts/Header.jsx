import Link from "next/link";
import { classNames, getRandomInt } from "../../functions";
import VitrinLogoSvg from "./VitrinLogoSvg";
import { Fragment } from "react";
import { Popover, Transition } from "@headlessui/react";
import { MenuIcon, XIcon } from "@heroicons/react/outline";
import { ChevronDownIcon } from "@heroicons/react/solid";

const resources = [
  { id: 1, name: "Products", href: "/products" },
  { id: 2, name: "Random Product", href: "/products" },
  // {id: 3,  name: "Docs", href: "/" },
  // {id: 4,  name: "Company", href: "/" },
];

export default function Header() {
  let max = 20;

  return (
    <Popover className="relative bg-white">
      <div className="flex justify-between items-center px-4 py-6 sm:px-6 md:justify-start md:space-x-10">
        <div>
          <Link href="/">
            <a className="">
              <span className="sr-only">Vitrin</span>
              <VitrinLogoSvg />
            </a>
          </Link>
        </div>
        <div className="-mr-2 -my-2 md:hidden">
          <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
            <span className="sr-only">Open menu</span>
            <MenuIcon className="h-6 w-6" aria-hidden="true" />
          </Popover.Button>
        </div>
        <div className="hidden md:flex-1 md:flex md:items-center md:justify-between">
          <Popover.Group as="nav" className="flex space-x-10">
            <Link href="/products">
              <a className="text-base font-medium text-gray-500 hover:text-gray-900">
                Products
              </a>
            </Link>
            <Link href={`/products/${getRandomInt(max)}`}>
              <a className="text-base font-medium text-gray-500 hover:text-gray-900">
                Random Product
              </a>
            </Link>

            <Popover className="relative">
              {({ open }) => (
                <>
                  <Popover.Button
                    className={classNames(
                      open ? "text-gray-900" : "text-gray-500",
                      "group bg-white rounded-md inline-flex items-center text-base font-medium hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    )}
                  >
                    <span>More</span>
                    <ChevronDownIcon
                      className={classNames(
                        open ? "text-gray-600" : "text-gray-400",
                        "ml-2 h-5 w-5 group-hover:text-gray-500"
                      )}
                      aria-hidden="true"
                    />
                  </Popover.Button>

                  <Transition
                    as={Fragment}
                    enter="transition ease-out duration-200"
                    enterFrom="opacity-0 translate-y-1"
                    enterTo="opacity-100 translate-y-0"
                    leave="transition ease-in duration-150"
                    leaveFrom="opacity-100 translate-y-0"
                    leaveTo="opacity-0 translate-y-1"
                  >
                    <Popover.Panel className="absolute z-10 left-1/2 transform -translate-x-1/2 mt-3 px-2 w-screen max-w-xs sm:px-0">
                      <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
                        <div className="relative grid gap-6 bg-white px-5 py-6 sm:gap-8 sm:p-8">
                          {resources.map((item) => (
                            <Link
                              key={item.name}
                              href={
                                item.id === 2
                                  ? `${item.href}/${getRandomInt(max)}`
                                  : item.href
                              }
                            >
                              <a>
                                <Popover.Button className="-m-3 p-3 w-full text-left block rounded-md hover:bg-gray-200">
                                  <p className="text-base font-medium text-gray-900">
                                    {item.name}
                                  </p>
                                </Popover.Button>
                              </a>
                            </Link>
                          ))}
                        </div>
                      </div>
                    </Popover.Panel>
                  </Transition>
                </>
              )}
            </Popover>
          </Popover.Group>
          <div className="flex items-center md:ml-12">
            <Link href="/">
              <a className="text-base font-medium text-gray-500 hover:text-gray-900">
                Sign in
              </a>
            </Link>
            <Link href="/">
              <a className="ml-8 inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700">
                Sign up
              </a>
            </Link>
          </div>
        </div>
      </div>

      <Transition
        as={Fragment}
        enter="duration-200 ease-out"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="duration-100 ease-in"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        <Popover.Panel
          focus
          className="absolute z-50 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden"
        >
          <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
            <div className="pt-5 pb-6 px-5">
              <div className="flex items-center justify-between">
                <div>
                  <Link href="/">
                    <a className="">
                      <span className="sr-only">Vitrin</span>
                      <VitrinLogoSvg />
                    </a>
                  </Link>
                </div>
                <div className="-mr-2">
                  <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                    <span className="sr-only">Close menu</span>
                    <XIcon className="h-6 w-6" aria-hidden="true" />
                  </Popover.Button>
                </div>
              </div>
            </div>
            <div className="py-2 px-5">
              <div className="grid grid-cols-2 gap-4">
                {resources.map((item) => (
                  <Link
                    key={item.name}
                    href={
                      item.id === 2
                        ? `${item.href}/${getRandomInt(max)}`
                        : item.href
                    }
                  >
                    <a>
                      <Popover.Button className="text-base font-medium text-gray-900 hover:text-gray-700">
                        {item.name}
                      </Popover.Button>
                    </a>
                  </Link>
                ))}
              </div>
              <div className="mt-6">
                <Link href="/">
                  <a>
                    <Popover.Button className="w-full flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700">
                      Sign up
                    </Popover.Button>
                  </a>
                </Link>

                <p className="mt-6 text-center text-base font-medium text-gray-500">
                  Existing customer?
                  <Link href="/">
                    <a>
                      <Popover.Button className="text-indigo-600 hover:text-indigo-500">
                        Sign in
                      </Popover.Button>
                    </a>
                  </Link>
                </p>
              </div>
            </div>
          </div>
        </Popover.Panel>
      </Transition>
    </Popover>
  );
}
