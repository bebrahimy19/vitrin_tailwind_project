import Image from "next/image";
import Link from "next/link";

export default function ProductsList({ productsData }) {
  return (
    <div className="bg-white">
      <div className="max-w-2xl mx-auto py-2 sm:py-8 px-4 sm:px-6 lg:max-w-7xl lg:px-8">
        <h2 className="text-xl font-bold text-gray-900">Products List</h2>

        <div className="mt-8 grid grid-cols-1 gap-y-10 cm:gap-y-20 sm:grid-cols-2 sm:gap-x-6 lg:grid-cols-4 xl:gap-x-8">
          {productsData.map((product) => (
            <div className="flex flex-col justify-between" key={product.id}>
              <div className="relative">
                <div className="relative w-full h-72 rounded-lg overflow-hidden">
                  <div className="w-full h-full object-center object-cover">
                    <Image
                      layout="fill"
                      src={product.image}
                      alt={product.category}
                    />
                  </div>
                </div>
                <div className="relative mt-4">
                  <h3 className="text-sm font-medium text-gray-900">
                    {product.title}
                  </h3>
                  {/* <p className="mt-1 text-sm text-gray-500">{product.color}</p> */}
                </div>
                <div className="absolute top-0 inset-x-0 h-72 rounded-lg p-4 flex items-end justify-end overflow-hidden">
                  <div
                    aria-hidden="true"
                    className="absolute inset-x-0 bottom-0 h-36 bg-gradient-to-t from-black opacity-50"
                  />
                  <p className="relative text-lg font-semibold text-white">
                    ${product.price}
                  </p>
                </div>
              </div>
              <div className="mt-6">
                <Link href={`/products/${product.id}`} passHref>
                  <a
                    // target="_blank"
                    className="relative flex bg-gray-100 border border-transparent rounded-md py-2 px-8 items-center justify-center text-sm font-medium text-gray-900 hover:bg-gray-200"
                  >
                    Details
                    <span className="sr-only">, {product.category}</span>
                  </a>
                </Link>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
