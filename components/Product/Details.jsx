import { RadioGroup } from "@headlessui/react";
import React, { useState } from "react";
import { classNames } from "../../functions";

const product = {
  name: "Nike",
  colors: [
    {
      name: "Washed Black",
      bgColor: "bg-gray-400",
      selectedColor: "ring-gray-400",
    },
    { name: "White", bgColor: "bg-white", selectedColor: "ring-gray-200" },
    {
      name: "Washed Red",
      bgColor: "bg-red-500",
      selectedColor: "ring-red-500",
    },
    {
      name: "Washed Green",
      bgColor: "bg-green-500",
      selectedColor: "ring-green-500",
    },
    {
      name: "Washed Yellow",
      bgColor: "bg-yellow-400",
      selectedColor: "ring-yellow-400",
    },
  ],
};

export default function Details() {
  const [selectedColor, setSelectedColor] = useState(product.colors[0]);

  return (
    <div className="flex justify-between my-10">
      {/* Colors */}
      <div className="w-1/2">
        <h3 className="text-xs sm:text-sm font-medium text-gray-700">COLOR</h3>

        <RadioGroup
          value={selectedColor}
          onChange={setSelectedColor}
          className="mt-2"
        >
          <RadioGroup.Label className="sr-only">
            Choose a color
          </RadioGroup.Label>
          <div className="flex items-center space-x-3">
            {product.colors.map((color) => (
              <RadioGroup.Option
                key={color.name}
                value={color}
                className={({ active, checked }) =>
                  classNames(
                    color.selectedColor,
                    active && checked ? "ring ring-offset-1" : "",
                    !active && checked ? "ring-2" : "",
                    "-m-0.5 relative p-0.5 rounded-full flex items-center justify-center cursor-pointer focus:outline-none"
                  )
                }
              >
                <RadioGroup.Label as="p" className="sr-only">
                  {color.name}
                </RadioGroup.Label>
                <span
                  aria-hidden="true"
                  className={classNames(
                    color.bgColor,
                    "h-6 w-6 border border-black border-opacity-10 rounded-full"
                  )}
                />
              </RadioGroup.Option>
            ))}
          </div>
        </RadioGroup>
      </div>
      {/* Sizes */}
      <div className="w-1/4 border-l-2 border-r-2 flex justify-center">
        <div>
          <label
            htmlFor="sizes"
            className="text-xs sm:text-sm font-medium text-gray-600"
          >
            SIZES
          </label>
          <select
            id="sizes"
            name="sizes"
            className="outline-none appearance-none cursor-pointer border-0 relative block w-full py-2 rounded-md placeholder-white text-bbbbbb font-medium text-sm sm:text-base"
          >
            <option>(UK 8)</option>
            <option>(UK 10)</option>
            <option>(UK 12)</option>
          </select>
        </div>
      </div>
      {/* Qty */}
      <div className="w-1/4 mx-3">
        <div>
          <label
            htmlFor="qty"
            className="text-xs sm:text-sm font-medium text-gray-600"
          >
            QTY
          </label>
          <select
            id="qty"
            name="qty"
            className="outline-none appearance-none cursor-pointer border-0 relative block w-full py-2 rounded-md placeholder-white text-bbbbbb font-medium text-sm sm:text-base"
          >
            <option>(1)</option>
            <option>(2)</option>
            <option>(3)</option>
          </select>
        </div>
      </div>
    </div>
  );
}
