import { ShareIcon, ShoppingCartIcon, StarIcon } from "@heroicons/react/solid";
import { classNames } from "../../functions";
import Details from "./Details";

export default function ProductInfo({ productData }) {
  return (
    <div className="lg:col-span-2 px-3 sm:px-8 py-4 sm:py-12">
      <div className="flex justify-between">
        <div>
          <h1 className="text-lg uppercase sm:text-3xl font-semibold tracking-tight text-gray-900">
            {productData.category}
          </h1>
          <h3 className="italic font-bold text-bbbbbb text-xs sm:text-sm mt-2">
            MINT GREEN
          </h3>
        </div>

        {/* Reviews */}
        <div className="mt-2">
          <h3 className="sr-only">Reviews</h3>
          <div className="flex items-center">
            <div className="flex items-center">
              {[0, 1, 2, 3, 4].map((rating) => (
                <StarIcon
                  key={rating}
                  className={classNames(
                    productData.rating.rate > rating
                      ? "text-red-500"
                      : "text-bbbbbb",
                    "h-5 w-5 flex-shrink-0 cursor-pointer hover:text-red-500"
                  )}
                  aria-hidden="true"
                />
              ))}
            </div>
            <p className="sr-only">{productData.rating.rate} out of 5 stars</p>
          </div>
        </div>
      </div>

      <div className="my-1">
        <h2 className="sr-only">Product information</h2>
        <div className="flex">
          <p className="text-lg font-semibold text-gray-800 line-through">
            ${productData.price}
          </p>
          <p className="text-lg font-semibold text-buttonTo mx-4">
            ${(productData.price * 0.9).toFixed(2)}
          </p>
        </div>
      </div>

      <div className="my-4 lg:my-12">
        <h3 className="font-semibold uppercase text-gray-700 text-xs sm:text-sm">
          Description
        </h3>

        <div
          className="text-xs sm:text-sm text-bbbbbb my-3"
          dangerouslySetInnerHTML={{ __html: productData.description }}
        />
      </div>

      <Details />

      <form className="my-4 lg:mt-12">
        <div className="flex justify-between">
          <button
            type="submit"
            className="shadow-buttonAddToCart bg-gradient-to-b from-buttonFrom to-buttonTo hover:from-buttonTo hover:to-buttonFrom border border-transparent rounded-md py-3 px-8 flex items-center justify-center text-base sm:text-lg font-medium text-white"
          >
            <ShoppingCartIcon
              className="h-6 w-6 flex-shrink-0 stroke-1 mx-2"
              aria-hidden="true"
            />
            ADD TO CART
          </button>

          <button
            type="button"
            className="ml-4 py-3 px-1 rounded-md flex items-center justify-center text-gray-400 hover:bg-gray-100 hover:text-gray-500"
          >
            <ShareIcon
              className="h-6 w-6 flex-shrink-0 stroke-1"
              aria-hidden="true"
            />
            <span className="sr-only">Add to favorites</span>
          </button>
        </div>
      </form>
    </div>
  );
}
