import ImageSlider from "./ImageSlider";
import ProductInfo from "./ProductInfo";

export default function ProductItem({ productData }) {
  return (
    <div className="bg-white my-10">
      <div className="sm:w-2/3 mx-auto shadow-productBox border rounded-lg">
        <div className="lg:grid lg:grid-cols-3 lg:gap-x-1 lg:items-start">
          {/* Image gallery */}
          <ImageSlider />

          {/* Product info */}
          <ProductInfo productData={productData} />
        </div>
      </div>
    </div>
  );
}
